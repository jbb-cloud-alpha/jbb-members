package org.jbb.cloud.members;

import org.jbb.cloud.email.api.v1.AsyncEmailService;
import org.jbb.cloud.secrets.api.v1.PasswordResource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication(scanBasePackageClasses = {App.class, PasswordResource.class,
    AsyncEmailService.class})
public class App extends SpringBootServletInitializer {

  public static void main(String[] args) {
    SpringApplication.run(App.class, args);
  }

}
