package org.jbb.cloud.members.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.jbb.cloud.members.api.v1.CreateMemberDto;
import org.jbb.cloud.members.api.v1.EditMemberDto;
import org.jbb.cloud.members.api.v1.MemberDto;
import org.jbb.cloud.members.model.Member;
import org.jbb.cloud.members.service.MemberService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/v1/members")
@Api(value = "/v1/members", tags = "Members", description = "Manage members")
public class MemberController {

  private final MemberService memberService;

  @ApiOperation("Returns all members")
  @RequestMapping(method = RequestMethod.GET)
  public List<MemberDto> getAllMembers() {
    return memberService.getAllMembers().stream()
        .map(member -> serialize(member))
        .collect(Collectors.toList());
  }

  @ApiOperation("Returns member by id")
  @RequestMapping(path = "/{id}", method = RequestMethod.GET)
  public MemberDto getMember(@PathVariable("id") String id) {
    return serialize(memberService.getMember(id).orElseThrow(() -> new IllegalArgumentException()));
  }

  @ApiOperation("Creates new member in system")
  @RequestMapping(method = RequestMethod.POST)
  public MemberDto createMember(@RequestBody @Valid CreateMemberDto createMemberDto) {
    Member newMember = deserialize(createMemberDto);
    return serialize(memberService.createMember(newMember, createMemberDto.getPassword()));
  }

  @ApiOperation("Updates existing member")
  @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
  public MemberDto editMember(@PathVariable("id") String id,
      @RequestBody @Valid EditMemberDto editMemberDto) {
    Member member = deserialize(editMemberDto, id);
    return serialize(memberService.editMember(member));
  }

  @ApiOperation("Deletes member from system")
  @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deleteMember(@PathVariable("id") String id) {
    memberService.deleteMember(id);
  }

  private MemberDto serialize(Member member) {
    return MemberDto.builder()
        .id(member.getId())
        .username(member.getUsername())
        .displayedName(member.getDisplayedName())
        .email(member.getEmail())
        .joinDateTime(member.getCreateDateTime())
        .build();
  }

  private Member deserialize(EditMemberDto editMemberDto, String id) {
    Member member = Member.builder()
        .username(editMemberDto.getUsername())
        .displayedName(editMemberDto.getDisplayedName())
        .email(editMemberDto.getEmail())
        .build();
    member.setId(id);
    return member;
  }

  private Member deserialize(CreateMemberDto createMemberDto) {
    return Member.builder()
        .username(createMemberDto.getUsername())
        .displayedName(createMemberDto.getDisplayedName())
        .email(createMemberDto.getEmail())
        .build();
  }

}
