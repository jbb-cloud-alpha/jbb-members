package org.jbb.cloud.members.service;

import java.util.List;
import java.util.Optional;
import org.jbb.cloud.members.model.Member;

public interface MemberService {

  Member createMember(Member newMember, String password);

  Member editMember(Member member);

  void deleteMember(String memberId);

  Optional<Member> getMember(String memberId);

  List<Member> getAllMembers();
}
