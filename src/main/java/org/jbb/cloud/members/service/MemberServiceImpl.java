package org.jbb.cloud.members.service;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jbb.cloud.email.api.v1.AsyncEmailService;
import org.jbb.cloud.email.api.v1.Email;
import org.jbb.cloud.members.model.Member;
import org.jbb.cloud.secrets.api.v1.PasswordResource;
import org.jbb.cloud.secrets.api.v1.SavePasswordDto;
import org.jbb.cloud.secrets.api.v1.exception.PasswordPolicyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class MemberServiceImpl implements MemberService {

  private final MemberRepository memberRepository;
  private final PasswordResource passwordsResource;
  private final AsyncEmailService asyncEmailService;

  @Override
  @Transactional
  public Member createMember(Member newMember, String password) {
    Member member = memberRepository.save(newMember);

    SavePasswordDto savePasswordDto = SavePasswordDto.builder()
        .memberId(member.getId())
        .password(password)
        .build();
    try {
      passwordsResource.savePassword(savePasswordDto);
    } catch (PasswordPolicyException e) {
      log.error("Cannot save password due to policy violation", e);
      throw new IllegalStateException(e);
    }

    sendWelcomeEmail(member);
    return member;
  }

  private void sendWelcomeEmail(Member member) {
    Email welcomeEmail = Email.builder()
        .recipientEmail(member.getEmail())
        .topic("Welcome to board")
        .content(String
            .format("Welcome %s! Thanks for registration on our board!", member.getDisplayedName()))
        .build();

    asyncEmailService.asyncSendEmail(welcomeEmail);
  }

  @Override
  @Transactional
  public Member editMember(Member member) {
    return memberRepository.save(member);
  }

  @Override
  @Transactional
  public void deleteMember(String memberId) {
    memberRepository.delete(memberId);
  }

  @Override
  public Optional<Member> getMember(String memberId) {
    return Optional.ofNullable(memberRepository.findOne(memberId));
  }

  @Override
  @Transactional(readOnly = true)
  public List<Member> getAllMembers() {
    return Lists.newArrayList(memberRepository.findAll());
  }
}
